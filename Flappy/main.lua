function love.load() -- Initilises Variables
  
  gamestate = "Title" --Gamestate "Title", "Game", "End"
  paused = false --Is the game paused?
  startTimer = true --Timer before game starts active ?
  score = 0 -- Players Score
  elapsedTime = 0 --Elapsed Time for timer
  countdownTimer = 0--Second variable for countdown timer
  timerStartCount = 7 --Starting seconds for timer
  inputAllowed = true --Can the player input commands
  flappyRotateCorrection = false -- Corrects the orgin of rotation after death 
  mouseX = -1 -- Mouse X pos when button 1 clicked
  mouseY = -1 -- Mouse Y pos when button 1 clicked
  dtHolder = 0  -- holds val of dt (elapsed time)
  
  -- Load Music
  mainGameMusic = love.audio.newSource("music/gamemusic.mp3")
  scoreMusic = love.audio.newSource("music/scorebeep.mp3")
  musicPlaying = false
  
  -- Menu Graphics 
  menu = {}
  menu.start = love.graphics.newImage("sprites/start.png")
  menu.logo = love.graphics.newImage("sprites/logo.png")
  menu.menu = love.graphics.newImage("sprites/menu.png")
  menu.restart = love.graphics.newImage("sprites/restart.png")
  menu.gameover = love.graphics.newImage("sprites/gameover.png")
  
  --Flappy Bird Animation
  frames = {} -- Table holding flappy images
  activeFrame = 0 --Frame that active
  currentFrame = 1 --Current frame
  flappySpritesheet = love.graphics.newImage("sprites/flappyspritesheet.png")-- Load flappy spritesheet
  frames[1] = love.graphics.newQuad(0,0,55,55,flappySpritesheet:getDimensions()) 
  frames[2] = love.graphics.newQuad(55,0,55,55,flappySpritesheet:getDimensions())
  frames[3] = love.graphics.newQuad(110,0,55,55,flappySpritesheet:getDimensions())
  activeFrame = frames[currentFrame] 
  
  --Load font sheet
  font = love.graphics.newImageFont("sprites/textfont.png",
    " abcdefghijklmnopqrstuvwxyz" ..
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ0" ..
    "123456789.,!?-+/():;%&`'*#=[]\"")
  love.graphics.setFont(font) --set font to fontsheet
  
  --Load and background
  background = love.graphics.newImage("sprites/bg.png")
  backgroundQuad = love.graphics.newQuad(1,1,720/2,1280/2,720/2,1280/2)
  
  --Load Clouds
  clouds = {}
  for x=0,1 do
  cloud = {}
  cloud.graph = love.graphics.newImage("sprites/clouds.png")
  cloud.PosX = love.graphics.getWidth() + (x * 350)
  cloud.PosY = 60
  table.insert(clouds,cloud)
end
  
   
    
  --Player Data
  flappy = {}
  flappy.img = love.graphics.newImage("sprites/flappy.png")
  flappy.posX = 125
  flappy.posY = 200
  flappy.oPosY = 0
  flappy.speed = 0
  flappy.gravity = 2.5
  flappy.rotation = 0
  flappy.dead = false
  flappy.collision = false
  flappy.highscore = 0
 
  --Highscore data
    highscore = {}
    
   
   if not love.filesystem.exists("scores.lua") then
    scores = love.filesystem.newFile("scores.lua")
    love.filesystem.write("scores.lua", "flappy.highscore\n=\n"..flappy.highscore)
   end
   for lines in love.filesystem.lines("scores.lua") do
    table.insert(highscore, lines)
   end
    flappy.highscore = highscore[3] 
-- Top pipes Data 
  pipesTop = {}
  for i=0,3 do
  pipeTop = {}
  pipeTop.graph = love.graphics.newImage("sprites/pipe.png")
  pipeTop.PosX =  love.graphics.getWidth() + 150 + 120 * i 
  pipeTop.alligned = false
  pipeTop.scale = 1
  pipeTop.PosY = 150
  table.insert(pipesTop,pipeTop)
end

-- Bot pipes Data
  pipesBot = {}
  for i=0,3 do
  pipeBot = {}
  pipeBot.graph = love.graphics.newImage("sprites/pipe.png")
  pipeBot.PosX =  love.graphics.getWidth() + 150 + 120 * i 
  pipeBot.scale = 1
  pipeBot.PosY = 500
  pipeCounted = false
  table.insert(pipesBot,pipeBot)
end
-- Array of scale of pipes
botScaleRef = {}
end

function reset_variables()
  -- Top pipes Data 
  pipesTop = {}
  for i=0,3 do
  pipeTop = {}
  pipeTop.graph = love.graphics.newImage("sprites/pipe.png")
  pipeTop.PosX =  love.graphics.getWidth() + 150 + 120 * i 
  pipeTop.alligned = false
  pipeTop.scale = 1
  pipeTop.PosY = 150
  table.insert(pipesTop,pipeTop)
end

-- Bot pipes Data
  pipesBot = {}
  for i=0,3 do
  pipeBot = {}
  pipeBot.graph = love.graphics.newImage("sprites/pipe.png")
  pipeBot.PosX =  love.graphics.getWidth() + 150 + 120 * i 
  pipeBot.scale = 1
  pipeBot.PosY = 500
  pipeCounted = false
  table.insert(pipesBot,pipeBot)
end
-- Array of scale of pipes
botScaleRef = {}
  
  flappy.posX = 125
  flappy.posY = 200
  flappy.oPosY = 0
  flappy.speed = 0
  flappy.gravity = 2.5
  flappy.rotation = 0
  flappy.dead = false
  flappy.collision = false
  
  paused = false --Is the game paused?
  startTimer = true --Timer before game starts active ?
  score = 0-- Players Score
  elapsedTime = 0 --Elapsed Time for timer
  countdownTimer = 0--Second variable for countdown timer
  timerStartCount = 7 --Starting seconds for timer
  inputAllowed = true --Can the player input commands
  flappyRotateCorrection = false -- Corrects the orgin of rotation after death 
  mouseX = -1 -- Mouse X pos when button 1 clicked
  mouseY = -1 -- Mouse Y pos when button 1 clicked
  dtHolder = 0  -- holds val of dt (elapsed time)
  
 
end
function love.mousepressed(x,y,button) -- Ckecks Mouse Presses
  if button == 1 then
    mouseX, mouseY = love.mouse.getPosition()
    end
  end

function state_title() -- Title Screen
  if musicPlaying == false then
    mainGameMusic:play()
    mainGameMusic:setLooping(true)
    musicPlaying = true
  end
  for i,v in ipairs(clouds) do
    v.PosX = v.PosX - 1.5
    if v.PosX <= -350 then 
      v.PosX = (love.graphics.getWidth()) 
    end
  end  
    startCollision = CheckCollision(love.graphics.getWidth()/2 - 40, love.graphics.getHeight()/2,80,40, mouseX,mouseY,1,1)
    if(startCollision) then
      gamestate = "Game"
      mouseX = -1
      mouseY= -1
      end
  end
function state_game() -- Game Screen
  if gamestate == "Game" and paused == false then
    for i,v in ipairs(clouds) do
      v.PosX = v.PosX - 0.8
      if v.PosX <= -350 then 
        v.PosX = (love.graphics.getWidth()) 
      end
    end
    if flappy.dead == false then
      flappy_wall_collision()
      start_timer()
      flappy_animation()
      flappy_jump_control()
      update_bottom_pipes()
      update_top_pipes()
      flappy_collision()
    end
     -- Flappy falling due to gravity and jumping
    flappy.posY = flappy.posY + flappy.gravity
    flappy.posY = flappy.posY + flappy.speed
  end
end
function state_end() -- DO vars yourself End Screen 
  end_menu_collision()
  flappy.posY = flappy.posY + flappy.gravity
 
    
    if score > tonumber(flappy.highscore) then
      flappy.highscore = score
      love.filesystem.write("scores.lua", "flappy.highscore\n=\n"..flappy.highscore)
      end
  
  for i,v in ipairs(clouds) do
    v.PosX = v.PosX - 1.5
    if v.PosX <= -350 then 
      v.PosX = (love.graphics.getWidth() + 350) 
    end
  end
end

function end_menu_collision() -- Button Collision for end screen
    menuCollision = CheckCollision(love.graphics.getWidth()/2 - 40, love.graphics.getHeight()/2,80,40, mouseX,mouseY,1,1)
    restartCollision = CheckCollision(love.graphics.getWidth()/2 - 40, love.graphics.getHeight()/2 + 80 ,80,40, mouseX,mouseY,1,1)
    if(menuCollision) then
      reset_variables()
      gamestate = "Title"
      mouseX = -1
      mouseY= -1
     end
    if (restartCollision) then
      reset_variables()
      gamestate = "Game"
      mouseX = -1
      mouseY= -1
     end
    end
    
function start_timer() -- Game start timer
  if startTimer == true then
    inputAllowed = false
    flappy.gravity = 0
    timerStartCount = timerStartCount - dtHolder
    countdownTimer = timerStartCount
    countdownTimer = math.ceil(countdownTimer)
    if flappy.posY ~= 200 then
      flappy.posY = 200
    end
    if countdownTimer < 0 then
      startTimer = false
    end
  else
    inputAllowed = true
    flappy.gravity = 2.5
  end
end

function flappy_animation() -- Flappy Animation
  elapsedTime = elapsedTime + dtHolder
  if (elapsedTime > 0.2) then
    if (currentFrame < 3) then
      currentFrame = currentFrame + 1
    else 
      currentFrame = 1
    end
    activeFrame = frames[currentFrame]
    elapsedTime = 0
  end
end
function flappy_jump() -- Flappy Jump
  flappy.oPosY = flappy.posY -- store starting position before jump
  flappy.gravity = 0 -- set gravity to 0
  flappy.speed = - 10 -- set jump speed
end
function flappy_jump_control() -- Flappy jump height check
  if flappy.oPosY > flappy.posY + 70 then 
    flappy.speed = 0
    flappy.gravity = 2.5
  end
end
function flappy_wall_collision() -- Flappy edge of screen Collision checks
  if flappy.posY > 1280/2 - 45 then -- If flappy hits floor
    flappy.posY = 1280/2 - 45
    flappy.dead = true  --flappy dies
  elseif flappy.posY < 0 then -- If flappy hits ceiling
    inputAllowed = false  --cant move flappy 
    flappy.speed = 0
    flappy.gravity = 2.5
  end
  if flappy.posY > 0 then 
    inputAllowed = true
  end
end
function update_bottom_pipes() -- Update bottom row of pipes
  for i,v in ipairs(pipesBot) do -- For all bottom pipes
    if v.PosX <= flappy.posX and v.pipeCounted == false then -- Add +1 to score and mark pipe as counted
      v.pipeCounted = true
      score = score + 1
      scoreMusic:play()
    end 
    v.PosX = v.PosX - 1.5 -- Move pipes on X axis

    if v.PosX == - 60 then -- If pipes move off left screen
      v.PosX = (love.graphics.getWidth()) + 60  -- Reset to right of screen
      v.scale = 0.7 + love.math.random() * (1.8 - 0.7) -- Randomise the scale
      botScaleRef = v.scale -- Hold a reference to the scale
      v.PosY = 640 - (v.scale * 150) -- Reposition the pipe 
      v.pipeCounted = false --Unmark the pipe as counted
    end
  
    -- Check Bot pipes for Collisions
    hitTestBot = CheckCollision(v.PosX, v.PosY , 60, 150, 125, flappy.posY - 5, 40, 40)
    if(hitTestBot) then -- If collided
    flappy.collision = true -- Tell us flappy collided
    end
  end
end
function update_top_pipes()   -- Update Top frow of pipes
  for i,v in ipairs(pipesTop) do -- for all top pipes
    v.PosX = v.PosX - 1.5 -- move on x axis off screen
    if v.alligned == false then -- if not alligned with bot pipes
      v.PosX = v.PosX + 60 -- fix allignment
      v.alligned = true -- mark as alligned
    end
    if v.PosX == - 60 then -- if pipes off screen
      v.scale = 1 / botScaleRef * 1.5 -- inverse of top pipes scale * 1.5
      v.PosY = 150 * v.scale -- reposition pipes
      v.PosX = (love.graphics.getWidth()) + 60 -- move pipes to right of screen
    end
    --Check top pipes collision
    hitTestTop = CheckCollision(v.PosX - 60, v.PosY - 150, 60, 150, 125, flappy.posY - 5, 40, 40)
    if(hitTestTop) then -- If collided
      flappy.collision = true -- mark flappy as collided
    end
  end
end
function flappy_collision() -- What happens after flappy collides
  if flappy.collision == true then -- If flappy is marked as collided
    if flappyRotateCorrection == false then -- if rotation isnt corrected
    flappy.posX = flappy.posX + 45 -- correct position based on rotation
    flappyRotateCorrection = true -- mark position as correct
    end
  flappy.posX = flappy.posX - 1.5 -- move flappy at same rate as pipes to simulate hit
  flappy.gravity = 6 -- Increase gravity so flappy falls fast to ground
  flappy.rotation = 1.5708 -- rotate flappy to face ground
  inputAllowed = false -- disable player input
  gamestate = "End" -- change gamescreen
  end
end
function love.update(dt) -- Update Function
  dtHolder = dt
  if gamestate == "Title" then
    state_title()
  elseif gamestate == "Game" then
    state_game()
  elseif gamestate == "End" then
    state_end()
  end
  game_screen()
end
function love.keyreleased(key) --- Keyboard pressed check
  if inputAllowed == true then 
    if key == "w" then -- jump function
      flappy_jump()
    end
    if key == "p" then -- changes pause variable
      if paused == true then
        paused = false
        mainGameMusic:play()
      else 
        paused = true
        mainGameMusic:pause()
      end
    end
  end
end
  
function love.draw() -- Draw Function
  love.graphics.draw(background, backgroundQuad, 0, 0) -- draw background
  if gamestate == "Game" then 
    game_screen()
  elseif gamestate == "Title" then
    title_screen()
  elseif gamestate == "End" then
    end_screen()
  end
end
function end_screen() -- Draw End Screen
  for i,v in ipairs(clouds) do -- Draw Clouds
    love.graphics.draw(v.graph, v.PosX, v.PosY,0,0.7,0.7)
  end
  for i,v in ipairs(pipesBot) do -- Draw Bottom Pipes
    love.graphics.draw(v.graph, v.PosX, v.PosY, 0,1 , v.scale)
  end
  for i,v in ipairs(pipesTop) do -- Draw Top Pipes
    love.graphics.draw(v.graph, v.PosX, v.PosY, math.pi, 1, v.scale)
  end 
  love.graphics.draw(flappySpritesheet,activeFrame,flappy.posX,flappy.posY,flappy.rotation) -- Draw flappy sprite sheet 
  love.graphics.draw(menu.gameover,-20,10,0,1.5,1.5) -- Draw Game Over
  love.graphics.draw(menu.menu, love.graphics.getWidth()/2 - 40, love.graphics.getHeight()/2) -- Draw menu button
  love.graphics.draw(menu.restart, love.graphics.getWidth()/2 - 40, love.graphics.getHeight()/2 + 80) -- Draw restart button
  love.graphics.print("Score: "..score, love.graphics.getWidth()/2 - 80, love.graphics.getHeight()/2 - 120, 0, 1.5,1.5)
   love.graphics.print("Highscore: "..flappy.highscore, love.graphics.getWidth()/2 - 80, love.graphics.getHeight()/2 - 80, 0, 1.5,1.5) -- Draw highscore
end
function title_screen() -- Draw Title Screen
  for i,v in ipairs(clouds) do
    love.graphics.draw(v.graph, v.PosX, v.PosY,0,0.7,0.7) -- Draw Clouds
  end
  love.graphics.draw(menu.start, love.graphics.getWidth()/2 - 40, love.graphics.getHeight()/2) -- Draw Start button
  love.graphics.draw(menu.logo, 20, 20) -- draw logo button
   love.graphics.print("Highscore: "..flappy.highscore, love.graphics.getWidth()/2 - 80, love.graphics.getHeight()/2 - 80, 0, 1.5,1.5) -- Draw highscore
end

function game_screen() -- Draw Game Screen
  for i,v in ipairs(clouds) do
    love.graphics.draw(v.graph, v.PosX, v.PosY,0,0.7,0.7) -- Draw Clouds
  end
  for i,v in ipairs(pipesBot) do
    love.graphics.draw(v.graph, v.PosX, v.PosY, 0,1 , v.scale) -- Draw bot pipes
  end
  for i,v in ipairs(pipesTop) do
    love.graphics.draw(v.graph, v.PosX, v.PosY, math.pi, 1, v.scale) -- Draw top pipes
  end
  love.graphics.draw(flappySpritesheet,activeFrame,flappy.posX,flappy.posY,flappy.rotation) -- draw flappy spitesheet 
  love.graphics.print("Score: "..score, 0,0) -- Draw score in  top left 
  love.graphics.print("Highscore: "..flappy.highscore, 0,20) -- Draw highscore in  top left 
  if countdownTimer >= 0 then -- if timer is not 0
  love.graphics.print("Starts in..."..countdownTimer,love.graphics.getWidth()/2 - 60,love.graphics.getHeight()/2) -- draw timer
  end
  if paused == true then -- if paused
    love.graphics.print("Paused", love.graphics.getWidth()/2 - 40, love.graphics.getHeight()/2) -- draw "paused"
  end
end

function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2) --  Function to check Collisions
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end